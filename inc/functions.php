<?php

/* CUSTOMIZER
 * ========================================================================== */

load_template( dirname( __FILE__ ) . '/customizer/customizer.php' );

/* ========================================================================== */




/* ========================================================================
 *            Section comment block
 * ======================================================================== */
function the_archives_header() {

	if (is_category()) : ?>
		<div class="inform">
			<h1><?php _e( 'Category', 'simplepuzzle' ); ?> &laquo;<?php single_cat_title(''); ?>&raquo;</h1>
			<?php if ( get_query_var('paged') == '' ) echo category_description(); ?>
		</div>
	<?php elseif( is_tag() ) : ?>
		<div class="inform">
			<h1><?php _e( 'Tag', 'simplepuzzle' ); ?> &laquo;<?php single_tag_title(); ?>&raquo;</h1>
			<?php if ( get_query_var('paged') == '' ) echo category_description(); ?>
		</div>
	<?php elseif (is_day()) : ?>
		<div class="inform">
			<h1><?php _e( 'Day archives:', 'simplepuzzle' ); ?> <?php the_time('F jS, Y'); ?></h1>
		</div>
	<?php elseif (is_month()) : ?>
		<div class="inform">
			<h1><?php _e( 'Monthly archives:', 'simplepuzzle' ); ?> <?php the_time('F, Y'); ?></h1>
		</div>
	<?php elseif (is_year()) : ?>
		<div class="inform">
			<h1><?php _e( 'Year archives:', 'simplepuzzle' ); ?> <?php the_time('Y'); ?></h1>
		</div>
	<?php elseif (is_author()) : ?>
		<div class="inform">
			<h1><?php _e( 'Author archives', 'simplepuzzle' ); ?></h1>
			<?php if ( get_query_var('paged') == '' ) echo category_description(); ?>
		</div>
 	<?php elseif (isset($_GET['paged']) && !empty($_GET['paged'])) : ?> 
		<div class="inform">
 			<h1 class="arhivetitle"><?php _e( 'Archive', 'simplepuzzle' ); ?></h1>
		</div>	
	<?php endif;

}
/* ======================================================================== */





/* ========================================================================
 *            RECENT POSTS after entry content
 * ======================================================================== */
if ( !function_exists('the_recent_posts_simplepuzzle') ) :
function the_recent_posts_simplepuzzle() {

	echo get_recent_posts_simplepuzzle();

}
endif;

if ( !function_exists('get_recent_posts_simplepuzzle') ) :
function get_recent_posts_simplepuzzle() {

	global $post;

	$res = '';

	$title = get_avd_option( 'title_recent_posts_onsingle' );
	$title = ( !empty($title) ) ? $title : __( 'Recent posts', 'simplepuzzle' );
	$title = '<p class="widget-title">'. $title .'</p>';

	$number_posts = get_avd_option( 'number_recent_posts_onsingle' );
	$number_posts = ( !empty($number_posts) ) ? $number_posts : 6;

	$source = get_avd_option( 'source_recent_posts_onsingle' );
	$source = ( !empty($source) ) ? $source : 'rand_all';

	$categories = get_the_category( $post->ID );
	$categories = json_decode( json_encode($categories), true );
	if ( function_exists('array_column') ) {
		$categories = array_column( $categories, 'term_id');
	} else {
		$categories = simplepuzzle_array_column( $categories, 'term_id');		
	}

echo '<pre>';
// var_dump(  );
echo '</pre>';

	// get item by current post category
	$args = array(
		'numberposts' => intval( $number_posts ), 
		'post_status' => 'publish',
	);
	if ( 'rand_cat' == $source || 'rand_all' == $source ) {
		$args['orderby'] = 'rand';
	}
	if ( 'last_cat' == $source || 'rand_cat' == $source ) {
		$args['category'] = implode(',', $categories);
	}
 	$items = wp_get_recent_posts( $args, OBJECT );
// var_dump($items);

 	$i = 0;
	$res .= '<div class="grid">';
	foreach ( $items as $recent_post ) {
		$i++;
		$new_cl = ( $i%2 != 0 ) ? ' newrow' : '';
		$res .= '<div class="recent-post small-post col6'. $new_cl .'">';
			$res .= '<a class="anoncethumb" href="'. get_permalink( $recent_post->ID ) .'">';
			if ( has_post_thumbnail($recent_post->ID) ) {
				$res .= '<span class="wrap-img">'. get_the_post_thumbnail( $recent_post->ID, 'thumbnail' ) .'</span>';
			}
			$res .= $recent_post->post_title .'</a>';
			$res .= '<div class="entry-meta"><span class="date">'. mysql2date( get_option('date_format'), $recent_post->post_date ) .'</span></div>';
		$res .= '</div>';
	}
	$res .= '</div>';


	return '<div class="recent-posts">'. $title . $res .'</div>';

}
endif;
/* ======================================================================== */


function simplepuzzle_array_column( $input, $column_key ) {

	$res = array();

	foreach ($input as $i => $val) {
		$res = $val[$column_key];
	}

	return $res;
}






/* ==========================================================================
 * Customize pagination
 *
 *	@param 	$total (true) = show total pages
 *	@param 	$prev ( « Prev ) = PREV link text
 *	@param 	$next ( Next » ) = NEXT link text
 *
 *	@return 	echo pagination
 *
 *  Example: 	<?php avd_the_pagination(); ?>
 *
 * ========================================================================== */
if ( ! function_exists( 'avd_the_pagination' ) ) :
function avd_the_pagination( $total = true, $prev = '', $next = '' ){
	global $wp_query;

	$prev = ( !empty($prev) ) ? $prev : __( '&laquo; Prev', 'simplepuzzle');
	$next = ( !empty($next) ) ? $next : __( 'Next &raquo;', 'simplepuzzle');

	$big = 999999999;
	$currentpage = max( 1, get_query_var('paged') );
	$maxpage = $wp_query->max_num_pages;

	$res = '<div class="avd-pagination">';
	if ( $maxpage > 1 && $total ) {
		$res .= '<span class="total">'. __('Page ', 'simplepuzzle') . $currentpage. _(' from ') .$maxpage.':  </span>';
	}
	$res .= paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => $currentpage,
		'total' => $maxpage,
		'mid_size' => 3,
		'prev_text' => $prev,
		'next_text' => $next
	));
	$res .= '</div>';
	echo $res;
}
endif;
/* ========================================================================== */




/* ========================================================================== *
 * Count post views
 *
 *	@param 	$whose string 	'all' - all visitors
 *							'guests' - only guests counted
 *							'users'  - only registered users counted
 *							Delault: 'all'
 *
 *	@param 	$key   string   what key use for post meta
 *							Delault: '_postviews'
 *
 * ========================================================================== */
function simplepuzzle_count_post_views( ) {

	global $user_ID, $post;
	static $post_views = false;

	$whose = 'all';
	$key   = '_postviews';

	if ( !is_singular() ) {
		return;
	}

	if ( $post_views )  {
		return true; 
	}

	$post_views = (int)get_post_meta( $post->ID, $key, true);
	$count_this = false;

	switch ( $whose ) {
		case 'all'  : $count_this = true; break;
		case 'guest': $count_this = ( empty($user_ID) )  ? true : false; break;
		case 'users': $count_this = ( !empty($user_ID) ) ? true : false; break;
	}

	if ( $count_this ) {

		$cnt = $post_views + 1;
		$upd = update_post_meta( $post->ID, $key, $cnt );
		if ( !$upd ) {
			add_post_meta( $post->ID, $key, 1, true );
		}
	}

}
add_action( 'wp_head', 'simplepuzzle_count_post_views' );
/* ========================================================================== */


/* ========================================================================== *
 * print or return post views
 * ========================================================================== */
function the_postviews_simplepuzzle( $echo = true, $key = '_postviews' ) {

	global $post;
 
	$post_views = get_post_meta( $post->ID, $key, true);

	if ( $echo ) {
		echo $post_views;
	} else {
		return $post_views;
	}

}
/* ========================================================================== */



/* ========================================================================== *
 * show recent posts for current post
 * ========================================================================== */
/*function the_recent_posts_simplepuzzle( $cnt = 6, $recentkey = 'views', $echo = true ) {

	$args = array(
		'numberposts' => $cnt,
		'order' => 'DESC',
		'category' => 0,
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'suppress_filters' => true 
	);

	if ( $recentkey ) {
		switch ( $recentkey ) {
			case 'views':
				$args['meta_key'] = '_postviews';
				$args['orderby']  = 'meta_value_num';
				break;
			default:
	    		$args['orderby'] = 'post_date';
				break;
		}
	}

    $recent_posts = wp_get_recent_posts( $args );

	$out ='<div class="featured-posts clearfix">';
	$out .='<span class="widget-title">'. __( 'Related posts', 'simplepuzzle' ) .'</span>';
	$out ='<div class="posts-768 clearfix">';

    foreach ($recent_posts as $id => $postitem ) {
    	?>
		<div class="the-same-post">
			<a class="anoncethumb"href="<?php echo get_permalink( $postitem['ID'] ); ?>">
				<?php if (has_post_thumbnail( $postitem['ID'] )) { ?>
				<span class="wrap-img">
					<?php echo get_the_post_thumbnail( $postitem['ID'], 'medium', 'class=bgc-trans' ); ?>
				</span>
				<?php } ?>
				<?php echo $postitem['post_title']; ?>						
			</a>
		<div class="entry-meta">
			<span>29 ноября 2015</span>
		</div>
		<?php
    }
	$out ='</div>';
	$out ='</div>';

	echo $out;

}*/
/* ========================================================================== */