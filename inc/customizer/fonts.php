<?php

ParseGoogleFonts::getInstance();


/* ========================================================================
 *            ParseGoogleFonts
 * ======================================================================== */

class ParseGoogleFonts
{

    /**
     * @var mixed
     */
    //экземпляр объекта
    protected static $_instance; 

    /**
     * @var string
     */
    private $url = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=alpha&fields=items(family%2Csubsets%2Cvariants%2Ccategory)&key=AIzaSyBf0Tre_tVEXG4NIQHKIaZ-j8sAiaiG3dM';

    /**
     * @var array
     */
    //в конструкторе добавим гугловские шрифты
    public $arr_fonts = array(
        'Arial'         => array('css' => "Arial, 'Helvetica Neue', Helvetica,sans-serif", 'url' => ''),
        'Courier_New'   => array('css' => "'Courier New', Courie", 'url' => ''),
        'Tahoma'        => array('css' => "Tahoma, Verdana, Segoe, sans-serif", 'url' => ''),
        'TimesNewRoman' => array('css' => "TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif", 'url' => ''),
        'Trebuchet_MS'  => array('css' => "'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif", 'url' => ''),
        'Verdana'       => array('css' => "Verdana, Geneva, sans-serif", 'url' => ''),
    );

    public static function getInstance()
    {
        // получить экземпляр данного класса
        if (self::$_instance === null) { // если экземпляр данного класса  не создан
            self::$_instance = new self; // создаем экземпляр данного класса
        }
        return self::$_instance; // возвращаем экземпляр данного класса
    }

    private function __construct()
    {
        // конструктор отрабатывает один раз при вызове DB::getInstance();
        $stored_fonts = get_option( 'wppz_google_fonts', null );

        if ( empty($stored_fonts) ) {
            $stored_fonts = $this->parse_all_from_google();
            if ( !is_array($stored_fonts) ) {
                exit($stored_fonts);
            }
        }
        $this->arr_fonts = array_merge( $this->arr_fonts, $stored_fonts );

    }
    
    /**
     * @return mixed
     */
    private function parse_all_from_google()
    {

        $cyr_fonts = array();

        $response = wp_remote_get($this->url);

        // проверка ошибки
        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();
            return __( 'Error parsing fonts list from Google Fonts API:', 'simplepuzzle'). $error_message;
        } 

        $fonts = json_decode(wp_remote_retrieve_body($response), true);
        $fonts = $fonts['items'];

        foreach ( $fonts as $key => $font ) {

            if ( in_array("cyrillic", $font['subsets']) ) {
                $name = str_replace(" ", "_", $font["family"]);
                $cyr_fonts[$name] = array(
                    'css' => "'". $font["family"]."', ". $font["category"], 
                    'url' => $this->create_url( $name, $font["subsets"], $font["variants"] ),
                );
            }

        }

        update_option( 'wppz_google_fonts', $cyr_fonts );           

        return $cyr_fonts;

    }   


    /**
     * @param  $name
     * @param  $subsets
     * @param  $variants
     * @return mixed
     */
    //для создания урл для стиля
    private function create_url($name, $subsets, $variants)
    {
        $out       = '';
        $name_plus = str_replace("_", "+", $name);
        $out .= $name_plus;

        $out .= ":";
        foreach ($variants as $variant) {
            if ( in_array($variant, array('regular','italic','700','700italic') ) ) {
                $out .= $variant .',';
            }
        }
        $out = substr($out, 0, -1);

        return $out;

    }

    private function __clone()
    { //запрещаем клонирование объекта модификатором private
    }

    private function __wakeup()
    { //запрещаем клонирование объекта модификатором private
    }

    /**
     * @return mixed
     */
    //все шрифты ключ ксс
    public function get_web_fonts()
    {

        // return $this->key_css($this->arr_fonts);
        return $this->arr_fonts;
    }    

    /**
     * @return mixed
     */
    //все шрифты ключ ксс
    public function get_web_fonts_css()
    {

        return $this->key_css($this->arr_fonts);
    }

    /**
     * @param  $arr
     * @return mixed
     */
    private function key_css($arr)
    {
        $newarr = array();
        foreach ($arr as $key => $value) {
            $newarr[$key] = $value['css'];
        }

        return $newarr;
    }

    /**
     * @return mixed
     */
    public function get_standart_fonts()
    {

        $font_standart = array_slice($this->arr_fonts, 0, 6);
        return $this->key_css($font_standart);
    }

    /**
     * @return mixed
     */
    //гугл шрифт ключ урл
    public function get_google_fonts()
    {

        $arr = array_slice($this->arr_fonts, 6);
        return $arr;
    }


    /**
     * @return mixed
     */
    //гугл шрифт ключ урл
    public function link_all_google_fonts()
    {

        $fonts = $this->get_google_fonts();
        $arr_exit = array();
        foreach ($fonts as $key => $value) {
            $arr_exit[$key] = $value['url'];
        }

        return $arr_exit;
    }

}
/* ======================================================================== */




/* ========================================================================
 *            generate lint to google fonts from theme mods
 * ======================================================================== */
function simplepuzzle_get_theme_fonts(){

    $fonts = ParseGoogleFonts::getInstance();
    $google_fonts = $fonts->link_all_google_fonts();
    $out = array();

    // get theme settings
    $b_font  = get_theme_mod('body_font', null);
    $h_font  = get_theme_mod('font_h1', null);
    $h2_font = get_theme_mod('font_h2', null);
    $h3_font = get_theme_mod('font_h3', null);
    $h4_font = get_theme_mod('font_h4', null);
    $h5_font = get_theme_mod('font_h5', null);
    $font_logo = get_theme_mod('font_logo', null);

    if($b_font){
        if(array_key_exists($b_font, $google_fonts) && !array_key_exists($b_font, $out)){
            $out[$b_font]= $google_fonts[$b_font];
        }
    }
    if($h_font){
        if(array_key_exists($h_font, $google_fonts) && !array_key_exists($h_font, $out)){
            $out[$h_font]= $google_fonts[$h_font];
        }
    }
    if($h2_font){
        if(array_key_exists($h2_font, $google_fonts) && !array_key_exists($h2_font, $out)){
            $out[$h2_font]= $google_fonts[$h2_font];
        }
    }
    if($h3_font){
        if(array_key_exists($h3_font, $google_fonts) && !array_key_exists($h3_font, $out)){
            $out[$h3_font]= $google_fonts[$h3_font];
        }
    }
    if($h4_font){
        if(array_key_exists($h4_font, $google_fonts) && !array_key_exists($h4_font, $out)){
            $out[$h4_font]= $google_fonts[$h4_font];
        }
    }
    if($h5_font){
        if(array_key_exists($h5_font, $google_fonts) && !array_key_exists($h5_font, $out)){
            $out[$h5_font]= $google_fonts[$h5_font];
        }
    }
    if($font_logo){
        if(array_key_exists($font_logo, $google_fonts) && !array_key_exists($font_logo, $out)){
            $out[$font_logo]= $google_fonts[$font_logo];
        }
    }

    // set default
    // if( !array_key_exists('Roboto', $out){
    if( empty($out) ){
        $out['PT_Sans']= $google_fonts['PT_Sans'];
    }

    return implode( '|', $out );
}
/* ======================================================================== */
