<?php 

load_template( plugin_dir_path( __FILE__ ). 'customizer-controls.php' );
load_template( plugin_dir_path( __FILE__ ). 'fonts.php' );



/* ==========================================================================
 *  customizer
 * ========================================================================== */
function true_customizer_init( $wp_customize ) {

    // global $google_fonts, $web_fonts;
    $fonts = ParseGoogleFonts::getInstance(); // инициализация экземпляра класса дл   работы
    // $web_fonts = $fonts->get_standart_fonts();
    $web_fonts = $fonts->get_web_fonts();
    // $google_fonts = $fonts->get_google_fonts();

    $true_transport = 'postMessage'; // для обновления не перегружая страницу


    /*----------  I N F O   B A R  ----------*/

    $wp_customize->add_section( 'infobar', 
        array(
            'title'     => __( 'Information sidebar', 'simplepuzzle' ),
            'priority'  => 2, 
            'description' => __( 'Customize blocks display and colors for top infomation bar', 'simplepuzzle' ) 
        )
    );

    $wp_customize->add_setting( 'true_infobar', array('default'  => 'true', 'transport' => $true_transport) );
    $wp_customize->add_control( 'true_infobar', 
        array(
            'section'   => 'infobar', 
            'label'      => __( 'Show information sidebar before header', 'simplepuzzle'),
            'type'      => 'checkbox', 
        )
    );

    // bg infoar
    $wp_customize->add_setting( 'bgcolor_infobar', array( 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bgcolor_infobar', 
            array(
                'section'    => 'infobar', 
                'label'      => __( 'Infobar background color', 'simplepuzzle'),
                'settings'   => 'bgcolor_infobar' 
            )
        )
    );


    // Цвета текста
    $wp_customize->add_setting( 'color_infobar', array( 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_infobar', 
            array(
                'section'    => 'infobar',
                'label'      => __( 'Infobar text color', 'simplepuzzle'),
                'settings'   => 'color_infobar' 
            )
        )
    );



    // how show infobar social icons
    $wp_customize->add_setting( 'infobar_social', array( 'default' => 'fl', 'transport' => $true_transport) );
    $wp_customize->add_control( 'infobar_social', 
        array(
            'section'  => 'infobar', 
            'label'    =>  __( 'Social links', 'simplepuzzle'),
            'type'     => 'select', 
            'choices'  => array( 
                'hide'   => __( 'Hide it', 'simplepuzzle' ), 
                'fl'   => __( 'Show left', 'simplepuzzle' ), 
                'fr'  => __( 'Show right', 'simplepuzzle' )
            )
        )
    );

    // show infobar search
    $wp_customize->add_setting( 'infobar_search', array( 'default' => 'fr', 'transport' => $true_transport) );
    $wp_customize->add_control( 'infobar_search', 
        array(
            'section'  => 'infobar', 
            'label'    =>  __( 'Search form', 'simplepuzzle'),
            'type'     => 'select', 
            'choices'  => array( 
                'hide'   => __( 'Hide it', 'simplepuzzle' ), 
                'fl'   => __( 'Show left', 'simplepuzzle' ), 
                'fr'  => __( 'Show right', 'simplepuzzle' )
            )
        )
    );

    // show infobar search
    $wp_customize->add_setting( 'infobar_message', array( 'default' => 'hide', 'transport' => $true_transport) );
    $wp_customize->add_control( 'infobar_message', 
        array(
            'section'  => 'infobar', 
            'label'    =>  __( 'Your message', 'simplepuzzle'),
            'type'     => 'select', 
            'choices'  => array( 
                'hide'   => __( 'Hide it', 'simplepuzzle' ), 
                'fl'   => __( 'Show left', 'simplepuzzle' ), 
                'fr'  => __( 'Show right', 'simplepuzzle' )
            )
        )
    );


    // how show infobar rihgt
    $wp_customize->add_setting( 'infobar_message_text', array( 'default' => '', 'transport' => $true_transport ) );
    $wp_customize->add_control( 'infobar_message_text', 
        array(
            'section'  => 'infobar', 
            'label'    =>  __( 'Enter text of your message', 'simplepuzzle'),
            'type'     => 'text', 
        )
    );

    

    /* -------------- L O G O T Y P E  ---------------- */

    // rename title setting
    $wp_customize->get_section( 'title_tagline' )->title = __( 'Logotype', 'simplepuzzle' );
    $wp_customize->remove_control( 'display_header_text');

    // change title setting transport
    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';

    // how show logo  --> theme_options_simplepuzzle[logotype]
    $wp_customize->add_setting( SMPZ_OPTION_NAME .'[logotype]', array( 'default'   => 'text', 'type' => 'option', 'transport' => $true_transport ) );
    $wp_customize->add_control( 'logotype', 
        array(
            'section'  => 'title_tagline', 
            'label'    =>  __( 'Site logo', 'simplepuzzle'),
            'type'     => 'select', 
            'settings' => SMPZ_OPTION_NAME . '[logotype]',
            'priority'  => 10,
            'choices'  => array( 
                'text'    => __( 'Text', 'simplepuzzle' ),
                'image'   => __( 'Image', 'simplepuzzle' ),
            )
        )
    );

    $wp_customize->get_control( 'blogname' )->priority = 11;

    $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
    $wp_customize->get_control( 'blogdescription' )->section = 'title_tagline';
    $wp_customize->get_control( 'blogdescription' )->priority = 11;

    // site descriptionа
    $wp_customize->add_setting( SMPZ_OPTION_NAME.'[showsitedesc]', array( 'type'   => 'option', 'transport' => $true_transport ) );
    $wp_customize->add_control( 'showsitedesc_control',
        array(
            'label'    =>  __( 'Show site description', 'simplepuzzle'),
            'settings' => SMPZ_OPTION_NAME.'[showsitedesc]',
            'section'  => 'title_tagline',
            'type'     => 'checkbox',
            'priority' => 11,
        )
    );
    
    // logo font
    $wp_customize->add_setting('font_logo', array('default'   => 'Roboto', 'type' => 'theme_mod', 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize, 'font_logo', 
            array(
                'section'  => 'title_tagline', 
                'label'    => __( 'Logo font', 'simplepuzzle' ),
                'priority' => 12,
                'type'     => 'select', 
                'choices'  => $web_fonts,//get_web_fonts()
            )
        )
    );

    // logo color
    $wp_customize->add_setting( 'logo_text_color', array( 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'logo_text_color', 
            array(
                'section'    => 'title_tagline', 
                'label'      => __( 'Logo color (for text)', 'simplepuzzle'),
                'settings'   => 'logo_text_color',
                'priority'   => 13,
            )
        )
    );

    // logo image
    $wp_customize->add_setting( SMPZ_OPTION_NAME . '[logo_image]', array('default' => '', 'type' => 'option', 'transport' => $true_transport));
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'logo_image',
            array(
                'label'    =>  __( 'Logo image', 'simplepuzzle'),
                'settings' => SMPZ_OPTION_NAME . '[logo_image]',
                'section'  => 'title_tagline',
                'priority' => 14,
            )
        )
    );



    /*----------  H E A D E R   ----------*/

    $wp_customize->get_section( 'header_image' )->title = __( 'Header', 'simplepuzzle');

    $wp_customize->get_control( 'header_image' )->priority = 70;
    


    // pages menu in header
    $wp_customize->add_setting( 'show_pages_menu', array('default' => 'true', 'transport' => $true_transport) );
    $wp_customize->add_control( 'show_pages_menu',
        array(
            'section'   => 'header_image', 
            'label'     => __( 'Show pages menu', 'simplepuzzle' ), 
            'type'      => 'checkbox',
            'priority'  => 10,
        )
     );

    // header bg
    $wp_customize->add_setting( 'bgcolor_header', array( 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bgcolor_header', 
            array(
                'section'    => 'header_image', 
                'label'      => __( 'Header background color', 'simplepuzzle'),
                'settings'   => 'bgcolor_header',
                'priority'   => 10,
            )
        )
    );

    // header image show
    $wp_customize->add_setting( 'bgimg_header', array( 'default' => 'image', 'transport' => $true_transport) );
    $wp_customize->add_control( 'bgimg_header', 
        array(
            'section'  => 'header_image', 
            'label'    =>  __( 'How set header images', 'simplepuzzle'),
            'type'     => 'select', 
            'settings' => 'bgimg_header',
            'priority'  => 10,
            'choices'  => array( 
                'image'     => __( 'Show original image', 'simplepuzzle' ),
                'repeat'    => __( 'Repeat image on background', 'simplepuzzle' ),
                'repeat-x'  => __( 'Horizontal repeat image on background', 'simplepuzzle' ),
                'center'    => __( 'Set image centered on background', 'simplepuzzle' ),
            )
        )
    );



    /*----------  B A C K G R O U N D  ----------*/
    
    $wp_customize->get_control( 'background_color' )->section = 'background_image';
    $wp_customize->get_section( 'background_image' )->title = __( 'Background', 'simplepuzzle' );




    /*----------  T E X T   C O L O R S  ----------*/

    $wp_customize->get_section('colors')->priority = 81;
    $wp_customize->get_section('colors')->title = __( 'Content and main colors', 'simplepuzzle' );
    $wp_customize->remove_control('header_textcolor');

// bg buttons
    $wp_customize->add_setting( 'button', array( 'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'button', 
            array(
                'label'      => __( 'Main elements color', 'simplepuzzle' ),
                'section'    => 'colors', 
                'settings'   => 'button', 
                'description' => __( 'This color will be used for the main site elements - buttons, bullets, quotes and other blocks.', 'simplepuzzle' ),
            )
        )
    );

// bg hover button
    $wp_customize->add_setting( 'button_hover', array( 'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'button_hover', 
            array(
                'label'      => __( 'Main elements color on hover', 'simplepuzzle' ),
                'section'    => 'colors', 
                'settings'   => 'button_hover',
                'description' => __( 'This color will be used for the main site elements when you hover over them.', 'simplepuzzle' ),
            )
        )
    );


    $wp_customize->add_setting('body_font', array('default' => 'PT_Sans', 'transport' => $true_transport));
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'body_font', 
            array(
                'section'  => 'colors', 
                'label'    => __( 'Content font', 'simplepuzzle' ),
                'type'     => 'select',
                'choices'  => $web_fonts
            )
        )
    );


// links color
    $wp_customize->add_setting('true_link_color', array( 'transport' => $true_transport));
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'true_link_color', 
            array(
                'label'      => __( 'Link color (in content)', 'simplepuzzle'),
                'section'    => 'colors', 
                'settings'   => 'true_link_color' 
            )
        )
    );



// color hover links
    $wp_customize->add_setting( 'true_link_color_hover',  array( 'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'true_link_color_hover', 
            array(
                'label'      => __( 'Hover link color (in content)', 'simplepuzzle' ),
                'section'    => 'colors', 
                'settings'   => 'true_link_color_hover' 
            )
        )
    );


// anons title color
    $wp_customize->add_setting( 'true_color_head_anonce', array( 'transport'   => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'true_color_head_anonce', 
            array(
                'label'      => __( 'Header color in summary', 'simplepuzzle' ),
                'section'    => 'colors', 
                'settings'   => 'true_color_head_anonce' 
            )
        )
    );





    /*----------  C O N T E N T   S E T T I N G S  ----------*/

 
   // content custom section
    $wp_customize->add_section(
        'headers_settings', 
        array(
            'title'     => __( 'Headers settings', 'simplepuzzle' ),
            'priority'  => 90, 
            'description' => __( 'Now you can set up custom color and fonts for headers. We recommend that you use no more than three different fonts to keep fast loading site.', 'simplepuzzle' ) 
        )
    );


// color h1
    $wp_customize->add_setting( 'color_h1', array( 'transport'   => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_h1', 
            array(
                'label'      => __( 'H1 color', 'simplepuzzle' ),
                'section'    => 'headers_settings', 
                'settings'   => 'color_h1' 
            )
        )
    );
 
 
// font h1
    $wp_customize->add_setting( 'font_h1',  array( 'default'   => 'PT_Sans',  'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'font_h1', 
            array(
                'section'  => 'headers_settings', 
                'label'    => __( 'H1 font', 'simplepuzzle' ),
                'type'     => 'select', 
                'choices'  => $web_fonts
            )
        )
    );


// color h2
    $wp_customize->add_setting( 'color_h2', array( 'transport' => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_h2', 
            array(
                'label'      => __( 'H2 color', 'simplepuzzle' ),
                'section'    => 'headers_settings', 
                'settings'   => 'color_h2' 
            )
        )
    );


// font h2
    $wp_customize->add_setting( 'font_h2',  array( 'default' => 'PT_Sans', 'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'font_h2', 
            array(
                'section'  => 'headers_settings', 
                'label'    => __( 'H2 font', 'simplepuzzle' ),
                'type'     => 'select', 
                'choices'  => $web_fonts
            )
        )
    );    


// color h3
    $wp_customize->add_setting( 'color_h3', array( 'transport'   => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_h3', 
            array(
                'label'      => __( 'H3 color', 'simplepuzzle' ),
                'section'    => 'headers_settings', 
                'settings'   => 'color_h3' 
            )
        )
    );


// font h3
    $wp_customize->add_setting( 'font_h3',  array( 'default'   => 'PT_Sans',  'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'font_h3', 
            array(
                'section'  => 'headers_settings', 
                'label'    => __( 'H3 font', 'simplepuzzle' ),
                'type'     => 'select', 
                'choices'  => $web_fonts
            )
        )
    );

// color h4
    $wp_customize->add_setting( 'color_h4', array( 'transport'   => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_h4', 
            array(
                'label'      => __( 'H4 color', 'simplepuzzle' ),
                'section'    => 'headers_settings', 
                'settings'   => 'color_h4' 
            )
        )
    );


// font h4
    $wp_customize->add_setting( 'font_h4',  array( 'default'   => 'PT_Sans',  'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'font_h4', 
            array(
                'section'  => 'headers_settings', 
                'label'    => __( 'H4 font', 'simplepuzzle' ),
                'type'     => 'select', 
                'choices'  => $web_fonts
            )
        )
    );


// colors h5- h6
    $wp_customize->add_setting( 'color_h5', array( 'transport'   => $true_transport) );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_h5', 
            array(
                'label'      => __( 'H5 & H6 color', 'simplepuzzle' ),
                'section'    => 'headers_settings', 
                'settings'   => 'color_h5' 
            )
        )
    );


// Шрифт h5
    $wp_customize->add_setting( 'font_h5',  array( 'default'   => 'PT_Sans',  'transport' => $true_transport ) );
    $wp_customize->add_control(
        new WP_Customize_Font_Control( $wp_customize,
            'font_h5', 
            array(
                'section'  => 'headers_settings', 
                'label'    => __( 'H5 - H6 font', 'simplepuzzle' ),
                'type'     => 'select', 
                'choices'  => $web_fonts
            )
        )
    );

 


    /*----------  F O O T E R  ----------*/

 
    $wp_customize->add_section(
        'site_footer',  
        array(
            'title'     => __( 'Footer', 'simplepuzzle' ),
            'priority'  => 90, 
            'description' => __( 'Customize footer', 'simplepuzzle' ) 
        )
    );


    $wp_customize->add_setting( SMPZ_OPTION_NAME.'[hide_wppuzzle_links]', array( 'type'   => 'option', 'default' => 'show', 'transport' => $true_transport ) );
    $wp_customize->add_control( 'hide_wppuzzle_links_control',
        array(
            'label'    =>  __( "Hide author's link", 'simplepuzzle'),
            'settings' => SMPZ_OPTION_NAME.'[hide_wppuzzle_links]',
            'section'  => 'site_footer',
            'type'     => 'select',
            'choices'  => array(
                'show'  => __( 'Show', 'simplepuzzle'),
                'hide'  => __( 'Hide', 'simplepuzzle'),
            )
        )
    );


// header widget color
    $wp_customize->add_setting( 'color_widget', array('transport' => $true_transport));
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_widget', 
            array(
                'label'      => __( 'Widget title background color', 'simplepuzzle' ),
                'section'    => 'site_footer', 
                'settings'   => 'color_widget' 
            )
        )
    );

    // footer copyrights
    $wp_customize->add_setting( 'true_footer_copyright_text', 
        array(
            'default'            => __( 'All rights reserved', 'simplepuzzle' ) . ' © ' .date('Y'), 
            'sanitize_callback'  => 'true_sanitize_copyright', 
            'transport'          => $true_transport
        )
    );
    $wp_customize->add_control(
        'true_footer_copyright_text', 
        array(
            'section'  => 'site_footer',  
            'label'    => __( 'Your copyright', 'simplepuzzle' ),
            'type'     => 'text' 
        )
    );
 
 
}
add_action( 'customize_register', 'true_customizer_init' );



// CUSTOM STYLES
// -----------------------------------------------------------------------------------
function true_customizer_css() { 

    $style = '';

    $fonts =  ParseGoogleFonts::getInstance();
    $fonts = $fonts->get_web_fonts_css();

// ---- header -----
    $bgcolor_header = get_theme_mod('bgcolor_header', null);
    if(!empty($bgcolor_header)){
        $style .= '#header{background-color:'. $bgcolor_header.';}';
    }    
    $bgimg_header = get_theme_mod('bgimg_header', null);
    $bgimg = get_header_image();
    if( !empty($bgimg_header) ){
        switch ($bgimg_header) {
            case 'repeat': 
                $style .= '#header{background-repeat:'. $bgimg_header.';background-image:url("'. $bgimg .'")}';
                break;
            case 'repeat-x': 
                $style .= '#header{background-repeat:'. $bgimg_header.';background-image:url("'. $bgimg .'")}';
                break;
            case 'center': 
                $style .= '#header{background-repeat:no-repeat;background-position:top center;background-image:url("'. $bgimg .'")}';
                break;
            default: break;
        }
        // $style .= '#header{background-color:'. $bgimg_header.';}';
    }


// ---- FONTS ----
    $b_font = get_theme_mod( 'body_font', null );
    if ( !empty($b_font) ) {
        $style .= 'body{font-family:'. $fonts[$b_font] .'}';
    }

    // logo font
    $font_logo = get_theme_mod('font_logo', null);
    if(!empty($font_logo)) {
        $style .= '.sitelogo-txt{font-family:'. $fonts[$font_logo] .';}';
    }


// ---- HEADERS ----

    $font_h1 = get_theme_mod('font_h1', null);
    if ( !empty($font_h1) ) {
        $style .= 'h1{font-family:'. $fonts[$font_h1] .'}';
    }

    $font_h2 = get_theme_mod('font_h2', null);
    if ( !empty($font_h2) ) {
        $style .= 'h2{font-family:'. $fonts[$font_h2] .'}';
    }

    $font_h3 = get_theme_mod('font_h3', null);
    if ( !empty($font_h3) ) {
        $style .= 'h3{font-family:'. $fonts[$font_h3] .'}';
    }

    $font_h4 = get_theme_mod('font_h4', null);
    if ( !empty($font_h4) ){
        $style .= 'h6{font-family:'. $fonts[$font_h4] .'}';
    } 

    $font_h5 = get_theme_mod('font_h5', null);
    if ( !empty($font_h5) ) {
        $style .= 'h5, h6{font-family:'. $fonts[$font_h5] .'}';
    }



    $color_header_anonce = get_theme_mod('true_color_head_anonce', null);
    if ( !empty($color_header_anonce) ){
        $style .= '.anons h2,.post-frontpage h2,.anons h2 a,.post-frontpage h2 a{color:'. $color_header_anonce .'; }';
    }


// ---- LINKS ----
    $color_link = get_theme_mod( 'true_link_color', null );
    if ( $color_link ) {
        $style .= '.entry a{color:'. $color_link .'}';
    }

    $color_link_hover = get_theme_mod( 'true_link_color_hover', null );
    if ( $color_link_hover ) {
        $style .= '.entry a:hover{color:'. $color_link_hover .'}.entry a::after{background-color:'. $color_link_hover .'}';
    }

// ------COLORS-----------

    // main color 
    $button = get_theme_mod('button', null);
    if( !empty($button) ) {        
        $style .= "a,h2,.social-share .like,.view-box a:hover,.anoncethumb:hover,.comment-content a,.left-wrap-two .small-post-bigimg .anoncethumb,.widget a:hover,#comments .widget-title,.logo,blockquote{color:$button}";
        $style .= "button,input[type='button'],input[type='reset'],input[type='submit'],.read-more,.more-link,.insider #submit-us,.wp-pagenavi .current,.post-nav-links .view:hover,.comment-form #submit,.bx-viewport,.discuss-title,#wlist #subsubmit,ol li:before,#footerbar .widget-title,.pagewrapper .searchform input.s, ul > li:before, ul > li:after,.main-menu a:hover, .main-menu .current-menu-item span, .main-menu .current-menu-item a,.page-numbers:hover,.infobar,.mobmenu,.mm-button.opened,.top-pages .sub-menu,.top-pages .children{background-color:$button}";
        $style .= ".page-numbers:hover,input[type=text]:focus,input[type=password]:focus,input[type=email]:focus,input[type=url]:focus,input[type=tel]:focus,input[type=date]:focus,input[type=datetime]:focus,input[type=datetime-local]:focus,input[type=time]:focus,input[type=month]:focus,input[type=week]:focus,input[type=number]:focus,input[type=search]:focus,textarea:focus{border-color:$button}";
        $style .= ".top-pages .sub-menu:before,.top-pages .children:before{border-bottom-color:$button}";

        $r = hexdec( substr($button, 1, 2) );
        $g = hexdec( substr($button, 3, 2) );
        $b = hexdec( substr($button, -2) );
        $style .= "blockquote{background-color:rgba($r,$g,$b,0.4)}";
        $style .= ".anoncethumb:hover .wrap-img:before{background-color:rgba($r,$g,$b,0.85)}";
    }

    // sedondary color 
    $button_hover = get_theme_mod('button_hover', null);
    if(!empty($button_hover)) {

        $style .= "a:hover,.entry a:hover,.comment a:hover,.left-wrap-two .small-post-bigimg .anoncethumb:hover,.bypostauthor .comment-author .fn,.bypostauthor .comment-author a,.entry-meta a:hover{color:$button_hover}";
        $style .= ".entry a::after,button:hover,input[type='button']:hover,input[type='reset']:hover,input[type='submit']:hover,.read-more:hover,.more-link:hover,.comment-form #submit:hover{background-color:$button_hover}";
    }

     // header color in summary
    $color_anonce= get_theme_mod('true_color_head_anonce', null);
    if(!empty($color_anonce)) {
         $style .= '.review_title a,.anoncethumb{color:'. $color_anonce .';}';
    }

     // widget title color
    $color_widget= get_theme_mod('color_widget', null);
    if( !empty($color_widget) ) {
         $style .= '#footerbar .widget-title{background-color:'. $color_widget .';}';
    }     

    // logo text color
    $logo_text_color= get_theme_mod('logo_text_color', null);
    if( !empty($logo_text_color) ) {
         $style .= '.logo{color:'. $logo_text_color .';}';
    }

//h1-h6
    $h1= get_theme_mod('color_h1', null);
    if(!empty($h1)) {
         $style .= 'h1{color:'. $h1 . ';}';
    }

    $h2= get_theme_mod('color_h2', null);
    if(!empty($h2)) {
         $style .= 'h2{color:'. $h2 . ';}';
    }

    $h3= get_theme_mod('color_h3', null);
    if(!empty($h3)) {
         $style .= 'h3{color:'. $h3 . ';}';
    }
    
    $h4= get_theme_mod('color_h4', null);
    if(!empty($h4)) {
         $style .= 'h4{color:'. $h4 . ';}';
    }
    
    $h5= get_theme_mod('color_h5', null);
    if(!empty($h5)) {
         $style .= 'h5, h6{color:'. $h5 . ';}';
    }


// ---- infobar -----
    $bgcolor_infobar = get_theme_mod('bgcolor_infobar', null);
    if(!empty($bgcolor_infobar)){
        $style .= '.infobar,.mobmenu,.mm-button.opened{background:'. $bgcolor_infobar.';}';
    }    

    $color_infobar = get_theme_mod('color_infobar', null);
    if(!empty($color_infobar)){
        $style .= '.infobar,.mobbar,#topsearch input.s,.infobar a,.top-message{color:'. $color_infobar.'}';
        $style .= '.infobar .search-button,.soc-item,.infobar .search-underform{fill-opacity:1;fill:'. $color_infobar.'}';
    }


// ---- FOOTER ----
    $footer_bg = get_theme_mod( 'footer_bg', null );
    if ( $footer_bg ) {
        $style .= '#footer{background:'. $footer_bg .'}';
    }

    $footer_text = get_theme_mod( 'footer_text', null );
    if ( $footer_text ) {
        $style .= '#footer,#footer p,#footer li{color:'. $footer_text .'}';
    }

    echo ( $style ) 
        ? "<!-- BEGIN Customizer CSS -->\n<style type='text/css' id='simplepuzzle-customizer-css'>$style</style>\n<!-- END Customizer CSS -->\n" 
        : "";

}
add_action( 'wp_head', 'true_customizer_css' ); 



function true_sanitize_copyright( $value ) {
    return strip_tags( stripslashes( $value ) ); 
}




//
/* ========================================================================
 *            script & styles for CUSTOMIZER 
 * ======================================================================== */
function simplepuzzle_customizer_live() {

    $fonts = ParseGoogleFonts::getInstance(); 
    $fonts_css = $fonts->get_web_fonts_css();
    $fonts_links = $fonts->link_all_google_fonts();

    wp_enqueue_script(
        'true-theme-customizer', 
        get_template_directory_uri() . '/inc/customizer/customizer-preview.js', // URL
        array( 'jquery', 'customize-preview' ), 
        null,
        true 
    );
    wp_localize_script( 'true-theme-customizer', 'fontsObj', $fonts_css );
    wp_localize_script( 'true-theme-customizer', 'opt_name', SMPZ_OPTION_NAME );

    // ADD google fonts
    $link = implode( '|', $fonts_links );
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family='. $link .'&subset=latin,cyrillic' );


}
add_action( 'customize_preview_init', 'simplepuzzle_customizer_live' );
/* ======================================================================== */


