<?php

$markup       = get_avd_option('use_schema');
$markup_type  = get_avd_option('schema_type'); 

$markup_lists  = get_avd_option('use_postslist_schema');
$markup_ltype  = get_avd_option('postslist_schema_type'); 


/* ========================================================================== *
 * markup itemtype (Article)
 * ========================================================================== */
function the_item_markup( $short = false ){
	global $post, $markup, $markup_lists, $markup_ltype, $markup_type; 

	if ( !$markup || ( $short && !$markup_lists ) ) {
		return;
	}

	// get mark up type or set default
	$name   = ( !$markup_type ) 
		? 'Article' 
		: $markup_type;

	// check comment and add reference
	$commcnt = get_comment_count( $post->ID );
	$reference = ( !$short && $commcnt['total_comments'] ) ? 'comments ' : '';	

	// if short - link to one publisher info
	$reference .= ( $short && $markup_lists )  ? 'publisher' : '';
	$reference = ( !empty($reference) ) ? ' itemref="'. $reference .'"' : '';

	$itemprop = '';
	if ( $short && $markup_lists ) {
		$itemprop = ( 'LiveBlogPosting' != $markup_ltype ) ? ' itemprop="hasPart"' : ' itemprop="liveBlogUpdate"';
	}

	// print itemtype
	echo ' itemscope'. $itemprop . $reference .' itemtype="http://schema.org/'. $name .'"';	

}
/* ========================================================================== * */




/* ========================================================================== *
 * posts LIST markup
 * ========================================================================== */
function the_list_markup( $what = '' ) {
	global $markup, $markup_lists, $markup_ltype;

	if ( !$markup_lists && 'publisher'!=$what ) {
		return false;
	}

	$mark = array('itemtype' => '', 'publisher' => '', 'siteinfo' => '' ); 

	$logo   = ( $markup && get_avd_option('markup_logo') ) ? get_avd_option('markup_logo') : get_template_directory_uri() .'/img/logo.jpg';
	$adress = ( $markup && get_avd_option('markup_adress') ) ? get_avd_option('markup_adress') : 'Russia';
	$phone  = ( $markup && get_avd_option('markup_telephone') ) ? get_avd_option('markup_telephone') : '+7 (000) 000-000-00';


	// get mark up type or set default
	$name = ( !$markup_ltype ) 
		? 'WebPage' 
		: $markup_ltype;

	// itemtype
	$mark['itemtype'] = ' itemscope itemtype="http://schema.org/'. $name .'"';	
	$mark['publisher'] = '
		<div itemscope itemtype="https://schema.org/Organization" id="publisher" itemprop="publisher">
			<meta itemprop="name" content="'. get_bloginfo('blogname') .'">
			<meta itemprop="address" content="'. $adress .'">
			<meta itemprop="telephone" content="'. $phone .'">
			<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
				<link itemprop="url contentUrl" href="'. $logo .'">
			</div>						
		</div>';
	
	if ( 'LiveBlogPosting' != $markup_ltype ) {
		$mark['siteinfo'] = '
			<meta itemprop="name" content="'. get_bloginfo('blogname') .'">
			<meta itemprop="description" content="'. get_bloginfo('description') .'">';
	}


	// return array or print info
	if ( empty($what) ) {
		return $mark;
	} 
	elseif ( 'itemtype' == $what ) {
		echo $mark['itemtype'];
	} 
	elseif ( 'publisher' == $what ) {
		echo $mark['publisher'];
	}
	elseif ( 'siteinfo' == $what ) {
		echo $mark['siteinfo'];
	}
		

}
/* ========================================================================== */




/* ==========================================================================
 *  ECHO markup Schema.org
 * ========================================================================== */
function the_markup_schemaorg( $type = '' ){
	global $post, $markup_lists;

	if ( 'short' == $type && !$markup_lists ) {
		return false;
	}

	$markup = ( is_single() && get_avd_option('use_schema') ) ? true : false;
	$img_size = ( is_singular() ) ? 'large' : 'full';
	$img_attr = ( has_post_thumbnail($post->ID) ) 
				? wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size ) 
				: array( get_template_directory_uri() .'/img/default.jpg', 80, 80 );

	?>	
	<!-- Schema.org Article markup -->
	<div class="markup">
		
		<meta itemscope itemprop="mainEntityOfPage" content="<?php the_permalink() ?>" />	
		
		<div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
			<link itemprop="url contentUrl" href="<?php echo $img_attr[0]; ?>">
			<meta itemprop="width" content="<?php echo $img_attr[1]; ?>">
			<meta itemprop="height" content="<?php echo $img_attr[2]; ?>">
		</div>
		
		<meta itemprop="datePublished" content="<?php the_time('c') ?>">
		<meta itemprop="dateModified"  content="<?php the_modified_time('c')?>"/>	
		<meta itemprop="author" content="<?php the_author() ?>">
		
		<?php 

		if ( 'short' != $type ) : 
			the_list_markup( 'publisher' );
		endif; ?>

	</div>
	<!-- END markup -->	
	<?php

}
/* ========================================================================== */





/* ========================================================================== *
 * Schema.org COMMENT callback
 * ========================================================================== */
function simplepuzzle_schemaorg_html5_comment( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
?>

	<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> itemprop="comment" itemscope itemtype="http://schema.org/Comment">
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">

			<footer class="comment-meta">
				<div class="comment-author">
					<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
					<b class="fn" itemprop="author"><?php echo comment_author_link(); ?></b>
				</div>

				<div class="comment-metadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
						<time datetime="<?php comment_time( 'c' ); ?>" itemprop="datePublished">
							<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'simplepuzzle' ), get_comment_date(), get_comment_time() ); ?>
						</time>
					</a>
					<?php edit_comment_link( __( 'Edit', 'simplepuzzle' ), '<span class="edit-link">', '</span>' ); ?>
				</div>

				<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'simplepuzzle' ); ?></p>
				<?php endif; ?>
			</footer>

			<div class="comment-content" itemprop="text">
				<?php comment_text(); ?>
			</div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 
							'add_below' => 'div-comment', 
							'depth' => $depth, 
							'max_depth' => $args['max_depth'] 
						)
				)); ?>
			</div>

		</div>

<?php
}
/* ========================================================================== */
