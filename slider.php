<?php 



global $exclude_posts;


/** BIG SLIDER
 * ============================================================== */

$where = get_avd_option('where_show_bigslider');
$where = (!is_array($where)) ? explode(',', $where) : $where;

$show = false;
if ( is_front_page() && is_singular() && in_array( 'home', $where) ) { $show = true; }
if ( is_home() && in_array( 'blog', $where) ) { $show = true; }
if ( is_archive() && in_array( 'archive', $where) ) { $show = true; }
if ( is_single() && in_array( 'post', $where) ) { $show = true; }
if ( is_page() && in_array( 'page', $where) ) { $show = true; }
if ( in_array( 'all', $where) ) { $show = true; }

if ( get_avd_option('show_bigslider') && $where && $show ) : 

	$items = array();
	$custom_arr = true;

	$source = get_avd_option('bigslider_showlastposts');
	$textlength = get_avd_option('slide_textlength');
	$number = get_avd_option('slide_number');
	$number = ( !empty($number) ) ? $number : 4;

	if ( 'recentposts' == $source ) {
		$items = wp_get_recent_posts( array( 'numberposts' => intval($number), 'post_status' => 'publish', ), OBJECT );
		$custom_arr = false;
	}
	else if ( 'featuredposts' == $source ) {
		$items = wp_get_recent_posts( 
					array(
						'meta_key' => '_postviews',
						'order' => 'DESC',
						'orderby' => 'meta_value_num',
						'numberposts' => intval( $number ), 
						'post_status' => 'publish',
					), 
					OBJECT
				);
		$custom_arr = false;
	} 
	else {
		$items = get_avd_option('custom_slider');
	}

	if ( $items ) { 
	?>
	<div class="first-slider clearfix">
		<ul class="bigslider bxslider">

		<?php foreach ( $items as $i => $slide ) :

			$img   = ( $custom_arr ) ? $slide['url']   : get_the_post_thumbnail( $slide->ID, array(700,0) );
			$title = ( $custom_arr ) ? $slide['title'] : $slide->post_title;
			$text  = ( $custom_arr ) ? $slide['text']  : wp_trim_words( wp_strip_all_tags($slide->post_content), 30, '');
			$link  = ( $custom_arr ) ? $slide['link']  : get_the_permalink( $slide->ID );

			if ( !$custom_arr ) {
				$exclude_posts[] = $slide->ID;
			}

		?>
				<li class="slide-item">
					<div class="slide-img">
						<?php if ( $custom_arr ) { ?>
							<img src="<?php echo $img; ?>" alt="<?php echo $title; ?>">
						<?php } else { 
							echo $img;
						} ?>
					</div>
					<div class="inside-info">
						<h2 class="slide-title"><?php echo $title; ?></h2>
						<?php if ( !empty($textlength) && !empty($text) ) : ?>
							<p class="slide-text"><?php echo $text; ?></p>
						<?php endif; ?>
						<div class="slide-button">
							<a href="<?php echo $link; ?>"class="more-link jread"><?php _e( 'Read more', 'simplepuzzle' ); ?></a>
						</div>
					</div>
				</li>
		<?php endforeach; ?>

		</ul>
	</div>
	<?php } ?>

<?php endif; 