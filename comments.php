<?php
if ( post_password_required() )
	return;

global $markup; 
// $comm_markup = ($markup) ? '  itemscope itemtype="http://schema.org/UserComments"' : '';

?>

<div id="comments" class="comments-area">

<?php if ( have_comments() ) : ?>
	<p class="widget-title"><?php _e('Comments', 'simplepuzzle'); ?></p>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  ?>
		<div class="comment-navigation">
			<div class="nav-prev"><?php previous_comments_link( __('&larr; Older Comments', 'simplepuzzle') ); ?></div>
			<div class="nav-next"><?php next_comments_link( __('Newer Comments &rarr;', 'simplepuzzle') ); ?></div>
		</div>
		<?php endif; ?>

		<ul class="comment-list">
			<?php 
				$comm_args = array( 
					'avatar_size' => '60',
					'callback' => 'simplepuzzle_html5_comment' 
				);
				if ( $markup ) {
					$comm_args['callback'] = 'simplepuzzle_schemaorg_html5_comment';
				}
				wp_list_comments( $comm_args ); 
			?>
		</ul><!-- .comment-list -->
	
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  ?>
		<div class="comment-navigation">
			<div class="nav-prev"><?php previous_comments_link( __('&larr; Older Comments', 'simplepuzzle') ); ?></div>
			<div class="nav-next"><?php next_comments_link( __('Newer Comments &rarr;', 'simplepuzzle') ); ?></div>
		</div>
		<?php endif; ?>

<?php endif; // have_comments() 

	$commenter = wp_get_current_commenter();

	$fields =  array(
		'author' => '<div class="row row-name"><input type="text" placeholder="'.__('Your Name','simplepuzzle').'" name="author" id="author" class="required" value="' . esc_attr( $commenter['comment_author'] ) . '" /></div>',		
		'email'  => '<div class="row row-email"><input type="text" placeholder="'.__('Your E-mail','simplepuzzle').'" name="email" id="email" class="required" value="'.esc_attr(  $commenter['comment_author_email'] ) . '" /></div>',
		'url'    => '<div class="row row-site"><input type="text" placeholder="'.__('Your Website','simplepuzzle').'" name="url" id="url" class="last-child" value="'. esc_attr( $commenter['comment_author_url'] ) . '"  /></div>'
	);
	$args = array(
	    'fields' => apply_filters( 'comment_form_default_fields', $fields ),
	    'comment_field' => '<div class="row row-message"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="'. __('Message','simplepuzzle') .'" aria-required="true"></textarea></div>',
	    'comment_notes_after' => ''
	);

	comment_form( $args ); ?>

</div><!-- #comments -->