<?php $s = get_search_query(); ?>
<form method="get" class="searchform" action="<?php echo home_url(); ?>/">
    <input type="text" value="" placeholder="<?php _e("Search", 'simplepuzzle'); ?>" name="s" class="s" />
    <div class="search-button-box">
		<svg class="search-underform"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#search" /></svg>
    	<input type="submit" class="submit search_submit" value="" />
    </div>
</form>
