

	<!-- BEGIN #sidebar -->
	<aside id="sidebar">
		<ul id="wlist" class="wlist clearfix">

        <?php if ( is_active_sidebar( 'sidebar' ) ) :
        	dynamic_sidebar( 'sidebar' ); 
        else : ?>

			<li class="widget widget_search">
				<?php get_search_form(); ?>
			</li>
			<?php if ( has_post_thumbnail()) {the_post_thumbnail(array(170, 170),array("class"=>"alignleft post_thumbnail"));} ?>				
			<?php wp_list_categories('title_li=<p class="widget-title">'. __("Categories", 'simplepuzzle') .'</p>');  ?>

		<?php endif; ?>
			
		</ul>
	</aside>
	<!-- END #sidebar -->
